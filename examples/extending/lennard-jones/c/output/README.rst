The files are:

* ``thermo.txt``: The output from ``md_nve.py``.

* ``thermo.txt.settings.external``: The output when
  using ``settings_external.rst``.

* ``thermo.txt.settings.internal``: The output when
  using ``settings.rst``.
