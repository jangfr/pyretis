Gromacs test
============

Simulation settings
-------------------
steps = 20

Engine settings
---------------
gmx = GMXCOMMAND
mdrun = GMXCOMMAND mdrun
input_path = gromacs_input
timestep = 0.002
subcycles = 5
gmx_format = g96
