
.. image:: https://gitlab.com/pyretis/pyretis/badges/master/pipeline.svg
    :target: https://gitlab.com/pyretis/pyretis/commits/master

.. image:: https://gitlab.com/pyretis/pyretis/badges/master/coverage.svg
    :target: https://gitlab.com/pyretis/pyretis/commits/master

.. image:: https://api.codacy.com/project/badge/Grade/5c452c3240574d9b90840e681a4ad3d7    
    :target: https://www.codacy.com/app/pyretis/pyretis?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=pyretis/pyretis&amp;utm_campaign=Badge_Grade

.. image:: https://badge.fury.io/py/pyretis.svg
    :target: https://badge.fury.io/py/pyretis

PyRETIS
=======

PyRETIS is a library for **rare event simulations**
with emphasis on methods based on transition interface sampling
replica exchange transition interface sampling.

PyRETIS is open source (see the COPYING file)
and can be interfaced with other simulation packages such as GROMACS or
CP2K.

The documentation is located at http://www.pyretis.org or it can
found in the docs directory of the source code.
