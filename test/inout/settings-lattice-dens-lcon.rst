Particles
---------
position = {'generate': 'fcc',
            'repeat': [3, 3, 3],
            'density': 0.9,
            'lcon': 1000.}


System
------
units = lj
