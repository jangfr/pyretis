# Cycle: 0, status: ACC
#     Time      Potential        Kinetic
         0      -0.964259       0.120697
         1      -0.963587       0.122573
         2      -0.962886       0.140784
         3      -0.962148       0.127610
         4      -0.961441       0.134963
         5      -0.960700       0.131557
         6      -0.960016       0.113981
         7      -0.959319       0.116699
         8      -0.958568       0.147229
         9      -0.957738       0.177780
        10      -0.956865       0.200620
        11      -0.955927       0.190805
        12      -0.954997       0.205138
        13      -0.954043       0.185818
        14      -0.953126       0.171426
        15      -0.952236       0.167464
        16      -0.951365       0.139017
        17      -0.950615       0.113361
        18      -0.949871       0.102657
        19      -0.949173       0.085959
        20      -0.948500       0.090363
        21      -0.947810       0.081043
        22      -0.947181       0.075524
        23      -0.946543       0.069684
        24      -0.945909       0.078022
        25      -0.945247       0.078339
        26      -0.944628       0.066861
        27      -0.944033       0.057663
        28      -0.943450       0.073338
        29      -0.942817       0.076379
        30      -0.942225       0.050288
        31      -0.941658       0.063558
        32      -0.941062       0.059678
        33      -0.940480       0.056867
        34      -0.939906       0.053253
        35      -0.939326       0.052707
        36      -0.938804       0.037324
        37      -0.938294       0.050117
        38      -0.937686       0.062915
        39      -0.937025       0.078342
        40      -0.936332       0.075814
        41      -0.935649       0.072834
        42      -0.935112       0.033492
        43      -0.934677       0.031021
        44      -0.934240       0.033252
        45      -0.933816       0.020407
        46      -0.933434       0.024931
        47      -0.933050       0.031710
        48      -0.932619       0.025991
        49      -0.932239       0.024141
        50      -0.931844       0.026868
        51      -0.931414       0.026426
        52      -0.931058       0.012211
        53      -0.930805       0.009978
        54      -0.930582       0.003791
        55      -0.930395       0.004428
        56      -0.930224       0.003405
        57      -0.930060       0.007473
        58      -0.929802       0.011091
        59      -0.929516       0.012175
        60      -0.929220       0.014370
        61      -0.928881       0.019154
        62      -0.928531       0.016251
        63      -0.928234       0.010460
        64      -0.927911       0.019532
        65      -0.927521       0.021883
        66      -0.927120       0.027581
        67      -0.926674       0.029211
        68      -0.926247       0.034167
        69      -0.925714       0.040846
        70      -0.925219       0.030914
        71      -0.924765       0.023205
        72      -0.924359       0.024087
        73      -0.923917       0.019279
        74      -0.923569       0.015780
        75      -0.923267       0.009370
        76      -0.923074       0.005248
        77      -0.922816       0.014697
        78      -0.922484       0.022072
        79      -0.922113       0.020934
        80      -0.921733       0.022896
        81      -0.921317       0.018013
        82      -0.920973       0.016351
        83      -0.920672       0.008458
        84      -0.920428       0.006281
        85      -0.920197       0.006097
        86      -0.919973       0.006567
        87      -0.919771       0.006304
        88      -0.919546       0.004012
        89      -0.919340       0.004952
        90      -0.919127       0.009032
        91      -0.918898       0.005524
        92      -0.918650       0.010092
        93      -0.918287       0.027392
        94      -0.917878       0.017666
        95      -0.917557       0.011628
        96      -0.917226       0.013538
        97      -0.916932       0.010256
        98      -0.916682       0.006487
        99      -0.916413       0.013894
       100      -0.916125       0.007249
       101      -0.915898       0.006057
       102      -0.915665       0.004948
       103      -0.915486       0.003272
       104      -0.915330       0.002498
       105      -0.915191       0.002574
       106      -0.915008       0.004459
       107      -0.914769       0.013276
       108      -0.914449       0.013152
       109      -0.914089       0.018383
       110      -0.913750       0.012273
       111      -0.913547       0.002573
       112      -0.913506       0.000004
       113      -0.913478       0.000257
       114      -0.913464       0.000012
       115      -0.913498       0.000060
       116      -0.913432       0.000668
       117      -0.913322       0.002569
       118      -0.913167       0.001372
       119      -0.913107       0.000167
       120      -0.913051       0.001617
       121      -0.912950       0.000564
       122      -0.912888       0.000449
       123      -0.912816       0.000357
       124      -0.912817       0.000008
       125      -0.912740       0.001850
       126      -0.912662       0.000319
       127      -0.912671       0.000260
       128      -0.912680       0.000000
       129      -0.912775       0.003468
       130      -0.912888       0.000088
       131      -0.912886       0.000531
       132      -0.912872       0.000024
       133      -0.912921       0.001714
       134      -0.913028       0.003122
       135      -0.913144       0.003915
       136      -0.913347       0.007470
       137      -0.913521       0.003109
       138      -0.913685       0.003710
       139      -0.913844       0.002540
       140      -0.913964       0.001942
       141      -0.914126       0.005278
       142      -0.914365       0.012003
       143      -0.914709       0.019753
       144      -0.915151       0.023728
       145      -0.915582       0.023201
       146      -0.916002       0.022066
       147      -0.916407       0.017662
       148      -0.916740       0.016480
       149      -0.917091       0.017020
       150      -0.917468       0.019833
       151      -0.917865       0.021471
       152      -0.918295       0.024033
       153      -0.918705       0.022251
       154      -0.919079       0.013776
       155      -0.919385       0.007521
       156      -0.919594       0.006868
       157      -0.919831       0.009307
       158      -0.920054       0.006959
       159      -0.920276       0.006244
       160      -0.920510       0.008657
       161      -0.920771       0.011423
       162      -0.920981       0.001736
       163      -0.921080       0.001555
       164      -0.921158       0.000826
       165      -0.921272       0.001819
       166      -0.921395       0.000970
       167      -0.921473       0.000845
       168      -0.921560       0.001640
       169      -0.921683       0.001768
       170      -0.921840       0.004339
       171      -0.922042       0.010042
       172      -0.922322       0.011073
       173      -0.922620       0.010478
       174      -0.923000       0.025356
       175      -0.923431       0.020946
       176      -0.923729       0.011333
       177      -0.924010       0.013441
       178      -0.924253       0.005206
       179      -0.924454       0.004080
       180      -0.924590       0.002019
       181      -0.924712       0.002308
       182      -0.924797       0.000013
       183      -0.924868       0.002497
       184      -0.924954       0.000778
       185      -0.925050       0.002547
       186      -0.925152       0.000958
       187      -0.925174       0.000083
       188      -0.925113       0.001980
       189      -0.925007       0.001518
       190      -0.924883       0.001425
       191      -0.924791       0.001837
       192      -0.924702       0.002414
       193      -0.924559       0.002184
       194      -0.924443       0.004912
       195      -0.924307       0.001488
       196      -0.924263       0.000285
       197      -0.924265       0.000004
       198      -0.924337       0.003203
       199      -0.924547       0.011225
       200      -0.924831       0.014133
       201      -0.925148       0.014026
       202      -0.925492       0.018853
       203      -0.925939       0.032534
       204      -0.926521       0.053482
       205      -0.927127       0.053450
       206      -0.927771       0.066235
       207      -0.928471       0.068963
       208      -0.929163       0.073610
       209      -0.929867       0.065081
       210      -0.930488       0.054471
       211      -0.931072       0.054430
       212      -0.931658       0.052724
       213      -0.932306       0.067976
       214      -0.932987       0.073449
       215      -0.933656       0.074048
       216      -0.934328       0.079408
       217      -0.935088       0.108669
       218      -0.935871       0.090350
       219      -0.936649       0.106736
       220      -0.937490       0.136291
       221      -0.938412       0.149473
       222      -0.939376       0.162331
       223      -0.940365       0.167466
       224      -0.941416       0.217105
       225      -0.942559       0.232167
       226      -0.943706       0.240423
       227      -0.944836       0.235757
       228      -0.945970       0.237266
       229      -0.947053       0.198700
       230      -0.948085       0.212373
       231      -0.949169       0.233343
       232      -0.950272       0.221056
       233      -0.951293       0.209785
       234      -0.952235       0.175133
       235      -0.953189       0.201936
       236      -0.954164       0.224309
       237      -0.955169       0.230725
       238      -0.956191       0.263153
       239      -0.957213       0.221152
       240      -0.958123       0.190097
       241      -0.959051       0.216123
       242      -0.960021       0.234508
       243      -0.960997       0.248169
       244      -0.961982       0.248115
       245      -0.962963       0.248457
       246      -0.963906       0.262046
# Cycle: 10, status: NCR
#     Time      Potential        Kinetic
         0      -0.965615       1.070023
         1      -0.963644       1.023026
         2      -0.961673       1.003506
         3      -0.959672       1.007929
         4      -0.957574       1.091850
         5      -0.955344       1.155215
         6      -0.953076       1.069559
         7      -0.950825       1.029043
         8      -0.948483       1.113717
         9      -0.946084       1.126728
        10      -0.943627       1.133776
        11      -0.941107       1.133810
        12      -0.938515       1.158275
        13      -0.935904       1.075895
        14      -0.933295       1.106280
        15      -0.930608       1.145069
        16      -0.927859       1.121101
        17      -0.925043       1.167239
        18      -0.922142       1.178806
        19      -0.919251       1.173790
        20      -0.916249       1.208386
        21      -0.913218       1.180255
        22      -0.910173       1.121876
        23      -0.907128       1.135592
        24      -0.904069       1.087570
        25      -0.901014       1.066105
        26      -0.897936       1.110920
        27      -0.894787       1.100792
        28      -0.891552       1.145663
        29      -0.888178       1.206857
        30      -0.884746       1.261815
        31      -0.881208       1.257847
        32      -0.877655       1.252975
        33      -0.874073       1.270120
        34      -0.870384       1.332403
        35      -0.866614       1.321393
        36      -0.862848       1.283224
        37      -0.859136       1.239633
        38      -0.855469       1.145658
        39      -0.851808       1.165366
        40      -0.848191       1.073370
        41      -0.844684       1.014372
        42      -0.841288       0.910889
        43      -0.837868       0.951230
        44      -0.834420       0.929048
        45      -0.831035       0.866818
        46      -0.827599       0.981710
        47      -0.823994       1.005924
        48      -0.820460       0.900051
        49      -0.816898       0.961246
        50      -0.813251       0.977876
        51      -0.809549       1.026340
        52      -0.805724       1.066365
        53      -0.801779       1.133927
        54      -0.797683       1.240804
        55      -0.793485       1.211061
        56      -0.789348       1.188775
        57      -0.785164       1.248482
        58      -0.780845       1.288934
        59      -0.776551       1.211960
        60      -0.772298       1.214275
        61      -0.767951       1.238809
        62      -0.763548       1.226184
        63      -0.759199       1.205973
        64      -0.754872       1.230190
        65      -0.750368       1.333304
        66      -0.745738       1.398135
        67      -0.740923       1.465462
        68      -0.736044       1.445968
        69      -0.731176       1.428946
        70      -0.726335       1.439957
        71      -0.721457       1.400962
        72      -0.716597       1.411162
        73      -0.711657       1.448995
        74      -0.706693       1.460463
        75      -0.701745       1.397343
        76      -0.696916       1.335339
        77      -0.692092       1.328396
        78      -0.687264       1.316518
        79      -0.682396       1.397524
        80      -0.677432       1.362090
        81      -0.672474       1.407689
        82      -0.667511       1.341049
        83      -0.662466       1.447683
        84      -0.657329       1.403864
        85      -0.652263       1.426044
        86      -0.647108       1.458683
        87      -0.642048       1.390229
        88      -0.637036       1.353356
        89      -0.632051       1.368273
        90      -0.627050       1.359695
        91      -0.622054       1.336752
        92      -0.617087       1.301738
        93      -0.612181       1.292827
        94      -0.607198       1.348629
        95      -0.602209       1.332086
        96      -0.597064       1.466323
        97      -0.591819       1.450652
        98      -0.586531       1.520036
        99      -0.581190       1.503663
       100      -0.575851       1.534845
       101      -0.570481       1.499051
       102      -0.565145       1.490945
       103      -0.559793       1.575179
       104      -0.554300       1.664433
       105      -0.548714       1.602240
       106      -0.543242       1.562066
       107      -0.537862       1.487032
       108      -0.532456       1.559216
       109      -0.526950       1.603566
       110      -0.521386       1.659180
       111      -0.515745       1.707459
       112      -0.509996       1.812755
       113      -0.504283       1.676814
       114      -0.498720       1.652327
       115      -0.493174       1.648103
       116      -0.487699       1.619351
       117      -0.482228       1.631737
       118      -0.476703       1.625117
       119      -0.471282       1.522265
       120      -0.466062       1.455108
       121      -0.460913       1.445257
       122      -0.455740       1.467774
       123      -0.450458       1.606415
       124      -0.445000       1.573174
       125      -0.439763       1.571040
       126      -0.434358       1.652182
       127      -0.428909       1.639635
       128      -0.423611       1.522448
       129      -0.418360       1.520463
       130      -0.413129       1.592834
       131      -0.407871       1.551021
       132      -0.402729       1.463000
       133      -0.397742       1.449037
       134      -0.392759       1.420183
       135      -0.387870       1.418469
       136      -0.383064       1.351702
       137      -0.378204       1.476632
       138      -0.373151       1.564191
       139      -0.368066       1.539796
       140      -0.362978       1.571869
       141      -0.357949       1.553215
       142      -0.352866       1.572669
       143      -0.347863       1.541090
       144      -0.342893       1.568748
       145      -0.337865       1.578959
       146      -0.332899       1.592382
       147      -0.328024       1.478812
       148      -0.323184       1.567376
       149      -0.318283       1.541635
       150      -0.313454       1.556260
       151      -0.308690       1.460605
       152      -0.304132       1.375257
       153      -0.299568       1.441382
       154      -0.294911       1.516088
       155      -0.290228       1.516402
       156      -0.285584       1.520620
       157      -0.280954       1.524290
       158      -0.276317       1.601102
       159      -0.271654       1.557108
       160      -0.266992       1.647841
       161      -0.262261       1.701679
       162      -0.257609       1.646556
       163      -0.252931       1.667683
       164      -0.248351       1.592232
       165      -0.243894       1.532155
       166      -0.239515       1.535429
       167      -0.235191       1.528861
       168      -0.230887       1.452160
       169      -0.226664       1.448455
       170      -0.222539       1.413901
       171      -0.218436       1.513640
       172      -0.214275       1.485792
       173      -0.210167       1.443466
       174      -0.206120       1.484138
       175      -0.202080       1.424141
       176      -0.198179       1.387750
       177      -0.194305       1.435120
       178      -0.190429       1.459126
       179      -0.186550       1.472629
       180      -0.182675       1.501213
       181      -0.178786       1.509458
       182      -0.174962       1.501889
       183      -0.171175       1.525402
       184      -0.167392       1.530606
       185      -0.163633       1.514790
       186      -0.159943       1.505552
       187      -0.156265       1.499853
       188      -0.152632       1.546833
       189      -0.149034       1.512120
       190      -0.145436       1.590004
       191      -0.141814       1.627451
       192      -0.138221       1.590145
       193      -0.134740       1.603012
       194      -0.131262       1.543239
       195      -0.127830       1.614482
       196      -0.124444       1.561361
       197      -0.121069       1.652024
       198      -0.117692       1.646416
       199      -0.114300       1.716245
       200      -0.110928       1.705357
       201      -0.107611       1.702595
       202      -0.104246       1.872293
       203      -0.100873       1.913706
       204      -0.097572       1.759305
       205      -0.094342       1.886371
       206      -0.091112       1.878221
       207      -0.087971       1.828060
       208      -0.084891       1.836775
       209      -0.081839       1.870779
       210      -0.078853       1.845001
       211      -0.075912       1.919648
       212      -0.072965       1.947085
       213      -0.070108       1.858397
       214      -0.067360       1.731843
       215      -0.064755       1.638417
       216      -0.062212       1.703185
       217      -0.059701       1.704392
       218      -0.057202       1.749383
       219      -0.054727       1.802894
       220      -0.052285       1.817662
       221      -0.049851       1.928953
       222      -0.047423       1.916237
       223      -0.045084       1.927660
       224      -0.042791       1.975669
       225      -0.040515       2.023382
       226      -0.038322       1.886829
       227      -0.036257       1.782091
       228      -0.034277       1.753723
       229      -0.032296       1.978074
       230      -0.030319       2.030459
       231      -0.028406       1.976120
       232      -0.026537       1.985272
       233      -0.024734       2.036818
       234      -0.022989       2.035506
       235      -0.021309       2.051437
       236      -0.019666       2.073362
       237      -0.018068       2.172321
       238      -0.016522       2.167639
       239      -0.015062       2.150371
       240      -0.013662       2.170520
       241      -0.012338       2.124933
       242      -0.011076       2.154921
       243      -0.009871       2.187846
       244      -0.008720       2.205536
       245      -0.007651       2.190661
       246      -0.006647       2.212096
       247      -0.005705       2.293806
       248      -0.004822       2.390352
       249      -0.004000       2.381077
       250      -0.003259       2.353775
       251      -0.002603       2.309321
       252      -0.002021       2.266755
       253      -0.001520       2.184523
       254      -0.001092       2.215329
       255      -0.000736       2.185988
       256      -0.000449       2.202133
       257      -0.000230       2.285994
       258      -0.000082       2.333013
       259      -0.000009       2.268511
       260      -0.000009       2.258768
       261      -0.000083       2.354231
       262      -0.000229       2.266712
       263      -0.000451       2.315409
       264      -0.000751       2.479210
       265      -0.001141       2.595295
       266      -0.001617       2.594868
       267      -0.002176       2.599970
       268      -0.002816       2.601030
       269      -0.003547       2.677140
       270      -0.004366       2.649215
       271      -0.005272       2.656431
       272      -0.006258       2.663265
       273      -0.007330       2.623074
       274      -0.008476       2.604360
       275      -0.009686       2.468411
       276      -0.010950       2.383255
       277      -0.012261       2.347379
       278      -0.013637       2.258675
       279      -0.015058       2.286449
       280      -0.016578       2.352421
       281      -0.018171       2.242805
       282      -0.019791       2.215405
       283      -0.021494       2.242661
       284      -0.023300       2.347021
       285      -0.025226       2.489645
       286      -0.027261       2.554351
       287      -0.029413       2.673920
       288      -0.031674       2.743705
       289      -0.034050       2.723355
       290      -0.036503       2.718234
       291      -0.039071       2.800102
       292      -0.041736       2.911355
       293      -0.044487       2.834787
       294      -0.047317       2.845584
       295      -0.050214       2.810032
       296      -0.053248       2.932547
       297      -0.056380       2.916611
       298      -0.059645       2.986277
       299      -0.063025       3.010340
       300      -0.066452       3.045961
       301      -0.070048       3.261216
       302      -0.073833       3.334529
       303      -0.077737       3.270704
       304      -0.081685       3.277586
       305      -0.085738       3.280563
       306      -0.089861       3.182149
       307      -0.094081       3.382493
       308      -0.098450       3.326765
       309      -0.102863       3.301635
       310      -0.107457       3.482871
       311      -0.112175       3.484425
       312      -0.117083       3.643414
       313      -0.122160       3.756184
       314      -0.127358       3.729087
       315      -0.132657       3.749919
       316      -0.138008       3.662435
       317      -0.143365       3.480622
       318      -0.148721       3.488043
       319      -0.154147       3.517421
       320      -0.159639       3.320718
       321      -0.165068       3.247590
       322      -0.170437       3.029625
       323      -0.175811       3.083142
       324      -0.181337       3.102463
       325      -0.186975       3.129377
       326      -0.192667       3.087349
       327      -0.198314       3.049891
       328      -0.204091       3.113860
       329      -0.209990       3.160761
       330      -0.216005       3.175694
       331      -0.222050       3.121368
       332      -0.228115       3.046186
       333      -0.234219       3.038500
       334      -0.240310       2.946313
       335      -0.246368       2.904906
       336      -0.252436       2.887953
       337      -0.258547       2.851773
       338      -0.264713       2.848311
       339      -0.270938       2.851862
       340      -0.277166       2.831992
       341      -0.283569       2.979802
       342      -0.290048       3.004600
       343      -0.296564       2.989568
       344      -0.303197       3.081326
       345      -0.309982       3.078661
       346      -0.316770       3.032198
       347      -0.323629       3.097618
       348      -0.330567       3.104621
       349      -0.337626       3.247788
       350      -0.344854       3.301327
       351      -0.352134       3.251590
       352      -0.359332       3.203529
       353      -0.366632       3.258290
       354      -0.374084       3.436806
       355      -0.381645       3.334676
       356      -0.389138       3.345985
       357      -0.396718       3.399277
       358      -0.404466       3.535173
       359      -0.412353       3.654499
       360      -0.420362       3.666605
       361      -0.428353       3.550897
       362      -0.436408       3.670067
       363      -0.444606       3.736871
       364      -0.452826       3.715206
       365      -0.461124       3.782893
       366      -0.469503       3.828338
       367      -0.477898       3.894577
       368      -0.486427       3.898726
       369      -0.494920       3.825272
       370      -0.503444       4.026588
       371      -0.512148       4.112833
       372      -0.520886       4.027481
       373      -0.529623       4.061210
       374      -0.538384       4.068082
       375      -0.547135       4.014071
       376      -0.555929       4.067412
       377      -0.564682       4.071433
       378      -0.573532       4.178447
       379      -0.582425       4.222575
       380      -0.591359       4.275088
       381      -0.600238       4.096086
       382      -0.609156       4.244959
       383      -0.618075       4.208007
       384      -0.626919       4.209443
       385      -0.635767       4.283804
       386      -0.644639       4.194867
       387      -0.653414       4.278471
       388      -0.662185       4.200282
       389      -0.671041       4.428740
       390      -0.679942       4.428232
       391      -0.688773       4.435223
       392      -0.697659       4.561152
       393      -0.706540       4.559011
       394      -0.715370       4.482702
       395      -0.723993       4.369309
       396      -0.732479       4.255958
       397      -0.740856       4.356373
       398      -0.749170       4.211284
       399      -0.757369       4.280267
       400      -0.765558       4.317866
       401      -0.773703       4.257467
       402      -0.781686       4.165644
       403      -0.789514       4.163783
       404      -0.797315       4.209353
       405      -0.805052       4.325483
       406      -0.812726       4.338247
       407      -0.820271       4.229234
       408      -0.827658       4.198999
       409      -0.834945       4.143918
       410      -0.842037       3.913485
       411      -0.848850       3.853942
       412      -0.855585       4.006751
       413      -0.862238       3.877858
       414      -0.868658       3.715860
       415      -0.874903       3.640694
       416      -0.880941       3.541816
       417      -0.886778       3.385662
       418      -0.892478       3.447816
       419      -0.898017       3.372374
       420      -0.903505       3.582126
       421      -0.908966       3.567185
       422      -0.914330       3.673430
       423      -0.919644       3.747561
       424      -0.924848       3.758326
       425      -0.929922       3.763321
       426      -0.934795       3.717446
       427      -0.939550       3.748070
       428      -0.944155       3.732818
       429      -0.948578       3.719391
       430      -0.952852       3.741308
       431      -0.956932       3.619977
       432      -0.960839       3.692544
       433      -0.964630       3.720641
       434      -0.968231       3.690902
       435      -0.971661       3.752819
       436      -0.974911       3.568212
       437      -0.977934       3.624429
       438      -0.980795       3.650591
       439      -0.983461       3.491617
       440      -0.985918       3.511426
       441      -0.988186       3.365838
       442      -0.990218       3.270273
       443      -0.992061       3.323141
       444      -0.993726       3.330181
       445      -0.995181       3.196942
       446      -0.996435       3.152776
       447      -0.997498       3.031209
       448      -0.998361       2.939722
       449      -0.999040       2.954023
       450      -0.999538       2.930368
       451      -0.999860       3.002317
       452      -0.999996       3.109546
       453      -0.999937       3.012601
