#     Time      Potential        Kinetic          Total    Temperature
         0      -0.165063       0.035000      -0.130063       0.070000
      1000      -0.806879       0.328694      -0.478185       0.657387
      2000      -0.760215       0.041118      -0.719097       0.082236
      3000      -0.951999       0.204093      -0.747906       0.408186
      4000      -0.992920       0.129780      -0.863141       0.259560
      5000      -0.965682       0.002716      -0.962966       0.005432
      6000      -0.967391       0.003806      -0.963584       0.007613
      7000      -0.997244       0.000065      -0.997180       0.000129
      8000      -0.990874       0.001018      -0.989856       0.002036
      9000      -0.818514       0.000006      -0.818508       0.000012
     10000      -0.999458       0.048668      -0.950790       0.097337
